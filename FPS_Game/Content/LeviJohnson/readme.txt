Author: Levi Johnson
Description: This folder is the designated work area for Levi Johnson within the FPS_Game project.
Any work outside of this folder by Levi Johnson must be approved by the other members of the group.